import {
  SimpleGrid,
  Text,
  Stack,
  Heading,
  Image,
  Flex,
  Box,
  chakra,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverBody,
  PopoverArrow,
} from '@chakra-ui/react'

import useMediaQuery from '../hook/useMediaQuery'
import SlideUpWhenVisible from '../hook/SlideUpWhenVisible'
import ReactGA from 'react-ga'

export default function AboutMe() {
  const isLargerThan800 = useMediaQuery(800)
  const handleHover = (event) => {
    ReactGA.event({
      category: 'hover',
      action: event,
    })
  }
  const MoreInfo = ({ text, content }) => {
    return (
      <>
        {' '}
        {isLargerThan800 ? (
          <Popover trigger="hover" placement="right" isLazy>
            <PopoverTrigger>
              <chakra.span
                onMouseOver={() => handleHover(`about_${text}`)}
                color="button1"
                cursor="help"
              >
                {text}
              </chakra.span>
            </PopoverTrigger>
            <PopoverContent bg="secondary" borderColor="button1" color="white">
              <PopoverArrow bg="button1" />
              <PopoverBody fontSize="sm" color="textPrimary">
                {content}
              </PopoverBody>
            </PopoverContent>
          </Popover>
        ) : (
          <Text as="span" color="button1">
            {text}
          </Text>
        )}{' '}
      </>
    )
  }

  return (
    <>
      <SimpleGrid columns={{ base: 1, md: 2 }} spacing={8}>
        <SlideUpWhenVisible>
          <Stack spacing={4}>
            <Heading fontFamily="Ubuntu" fontSize="2xl">
              ⚡ About Me
            </Heading>
            <Text
              color="textSecondary"
              fontSize={{ base: '14px', md: '16px' }}
              whiteSpace="pre-line"
            >
             I discovered my passion for IT during high school, specifically in the software engineering program. This led me to a keen interest in programming, especially web development. 
             <br />
             <br />
             ignited my interest in the field. I'm dedicated to learning programming because it refines my skills as a software engineer. 
            Currently, I work as a backend engineer at a 
             <MoreInfo
                text="Ticketing Company"
                content={
                  <Image 
                    w={306}
                    h={85}
                    src="https://imagizer.imageshack.com/img922/2734/azFxKm.png"
                    alt='linkedin Loket' 
                  />
                }
              >

             </MoreInfo>
             , and I've begun to explore the realms of 
             <MoreInfo
              text="system programming"
              content="As a software i dont want only use technology, i really want to know everything about behind it, and system programming help me to achieve that goal"
             >
               
             </MoreInfo>
             and
             <MoreInfo
              text="software architecture."
              content="Software architecture helps me understand how to build a system that is reliable and easy to scale and can also help me find bottlenecks that will occur in the system that will be built."
             >
              
              </MoreInfo> 
             As an engineer, my aspiration is to design end-to-end systems and gain a deeper understanding of the underlying processes.

             <br />
             <br />
             In my free time, I often immerse myself in the study of new technologies and enjoy reading books 📕.
            </Text>
          </Stack>
        </SlideUpWhenVisible>
        <SlideUpWhenVisible>
          <Flex alignItems="center" justifyContent="center" position="relative">
            <Box
              maxW={{ base: '300px', lg: '350px' }}
              maxH={{ base: '300px', lg: '350px' }}
            >
              <Image
                src="https://svgsilh.com/svg/26432.svg"
                filter="invert(0.1)"
                zIndex={3}
                position="absolute"
                top={0}
                right={0}
                w={{ base: '100px', lg: '150px' }}
                alt=""
              />
              <Image
                src="https://avatars.githubusercontent.com/u/34994341?v=4"
                w={{ base: '300px', lg: '350px' }}
                h={{ base: '300px', lg: '350px' }}
                borderRadius="50%"
                alt="Ahmad Haidar"
              />
            </Box>
          </Flex>
        </SlideUpWhenVisible>
      </SimpleGrid>
    </>
  )
}
